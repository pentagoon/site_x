
<?php
/**
 *
 */

defined('_JEXEC') or die;

/**

 */
abstract class Kk_KZLocalise
{
	/**
	
	 */
	public static function getPluralSuffixes($count)
	{
		if ($count == 0)
		{
			return array('0');
		}
		elseif ($count == 1)
		{
			return array('1');
		}
		else
		{
			return array('Тағы');
		}
	}

	/**
	 * Returns the ignored search words
	 *

	 *
	 * @since 1.6
	 */
	public static function getIgnoredSearchWords()
	{
		return array('және', 'ішінде', 'Үстінде');
	}

	/**
	
	 
	 */
	public static function getLowerLimitSearchWord()
	{
		return 3;
	}

	/**
	
	 */
	public static function getUpperLimitSearchWord()
	{
		return 20;
	}

	/**
	 
	 */
	public static function getSearchDisplayedCharactersNumber()
	{
		return 200;
	}
}
