<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php foreach ($list as $item) :  ?>
	<div>
         <a href="<?php echo $item->link;?>">
         <?php
		 	$img = json_decode($item->images);
		 ?>
          	<img src="<?php echo JUri::base().$img->image_intro;?>" alt="" />
                <?php if($item->params->get('show_title')) :?>
					<h3><?php echo $item->title;?></h3>
				<?php  endif; ?>	
					
        </a>   
                <p class="blog-shadow"></p>                                      	
            </div>
<?php endforeach; ?>

