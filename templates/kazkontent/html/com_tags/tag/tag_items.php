<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');


$items = $this->items;
$n = count($items);

?>


<div class="content">
	<?php if($items == FALSE || $n == 0) :?>
		<p><?php echo JText::_("COM_TAGS_NO_ITEMS"); ?></p>
	<?php endif;?>
	
	<?php foreach($items as $item) : ?>
		
		<div class="articles">
			<?php 
				$images = json_decode($item->core_images);
				
			
			?>
			<div class="articles-gen-img">
				<a href="<?php echo JRoute::_(TagsHelperRoute::getItemRoute($item->content_item_id,$item->core_alias,$item->core_catid,$item->core_language,$item->type_alias,$item->router))?>">
					<img src="<?php echo $images->image_intro;?>">
				</a>
				<p>
					<span>
						<?php echo JHtml::_('date',$item->core_publish_up,'d');?>
					</span>
					<?php echo JHtml::_('date',$item->core_publish_up,'M');?>
				</p>
			</div>
			
			<h1 class="bottom-line-h">
				<a href="<?php echo JRoute::_(TagsHelperRoute::getItemRoute($item->content_item_id,$item->core_alias,$item->core_catid,$item->core_language,$item->type_alias,$item->router))?>">
					<?php echo $this->escape($item->core_title);?>
				</a>
			</h1>
			<?php echo $item->event->afterDisplayTitle?>
			
			<?php if($this->params->get('tag_list_show_item_description')) :?>
				<?php echo $item->event->beforeDisplayContent;?>
				<?php echo JHtml::_('string.truncate',$item->core_body,$this->params->get('tag_list_item_maximum_characters'));?>
				<?php echo $item->event->afterDisplayContent;?>
			<?php endif;?>
			
		</div>
		
	<?php endforeach;?>
</div>