<?php

/**
* @copyright	Copyright (C) 2012 JoomSpirit. All rights reserved.
* Slideshow based on the JQuery script Flexslider
* @license		GNU General Public License version 2 or later
*/

defined('_JEXEC') or die;

?>

<div class="flexslider">
 
  <ul class="slides" <?php if ( $enable_minheight == 'yes' ) : ?>style="min-height:<?php echo $min_height ; ?>px;"<?php endif ; ?>>
  	<?php
  		foreach($listofimages as $item){
  			echo $item; 
  		}
  	?> 
  </ul>
  
  <?php if ( $slide_theme == 'true' ) : ?>
  <span class="slide-theme">
  	<span class="slide-theme-side slide-top-left"></span>
  	<span class="slide-theme-side slide-top-right"></span>
  	<span class="slide-theme-side slide-bottom-left"></span>
  	<span class="slide-theme-side slide-bottom-right"></span>
  </span>
  <?php endif; ?>
  
</div>

<script type="text/javascript">
  jQuery(window).load(function() {
    jQuery('.flexslider').flexslider({
        animation: "<?php echo $transition; ?>",
        easing:"linear",								// I disable this option because there was a bug with Jquery easing and Joomla 3.2.3
 		direction: "<?php echo $direction; ?>",        //String: Select the sliding direction, "horizontal" or "vertical"
		slideshowSpeed: <?php echo $pauseTime; ?>, 			// How long each slide will show
		animationSpeed: <?php echo $animSpeed; ?>, 			// Slide transition speed
    	directionNav: <?php if ($directionNav == 'false') { echo "false" ;} else { echo "true" ;} ?>,             
    	controlNav: <?php echo $controlNav ; ?>,    
    	pauseOnHover: <?php echo $pauseOnHover; ?>,
    	initDelay: <?php echo $initDelay; ?>,
    	randomize: <?php echo $randomize; ?>,
    	smoothHeight: false,
    	touch: false,
    	keyboardNav: true
    	
    });
  });
</script>