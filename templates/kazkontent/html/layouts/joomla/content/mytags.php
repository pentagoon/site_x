<?php
/**
 * @package     Joomla.Cms
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

JLoader::register('TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php');

?>
<?php if (!empty($displayData)) : ?>
<?php
$count = count($displayData);
$i = 1;
?>
<?php foreach ($displayData as $tag) :?>

	<a href="<?php echo JRoute::_(TagsHelperRoute::getTagRoute($tag->tag_id."-".$tag->alias))?>">
		<?php echo $tag->title;?>
	</a>
	<?php if($i < $count) :?>
		,&nbsp;
	<? endif; ?>

<?php $i++; ?>	
<?php endforeach; ?>
<?php endif; ?>
