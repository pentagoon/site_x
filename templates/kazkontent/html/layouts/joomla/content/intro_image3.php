<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$params  = $displayData->params;
?>
<?php $images = json_decode($displayData->images); ?>



<?php if (isset($images->image_intro) && !empty($images->image_intro)) : ?>
	
<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($displayData->slug,$displayData->catid)); ?>" class="project-linkF">
<div>
	<img alt="<?php echo htmlspecialchars($images->image_intro_alt);?>" src="<?php echo $images->image_intro;?>" class="img-fluid" >
	</div>
</a>
<?php endif; ?>







