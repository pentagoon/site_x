<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


function getUrlActive($path_)
{
    $document =& JFactory::getDocument();
    $currentUrl = basename($document->getBase());

    return basename($path_) === $currentUrl ? "active" : "";
}

?>
<? $count = 0 ?>
<? $size = count($list) - 1; ?>
<div class="header-menu">
    <div class="header-menu-panel transparent">
        <div class="container">
            <?php foreach ($list

            as $i => $item) : ?>


            <a href="<?php echo $item->link; ?>" class="project-link">
                <div>
                    <? $img = json_decode($item->images); ?>
                    <img src="<?php echo JUri::base() . $img->image_intro; ?>" alt=""
                         class="img-fluid news-img <?= getUrlActive($item->link); ?>"/>
                </div>
            </a>

            <? $count++ ?>

            <? if ($count % 5 == 0): ?>
            <div class="clearfix"></div>
        </div><!---------container------------->
    </div><!---------header-menu-panel transparent------------->
    <? if ($size != $i): ?>
    <div class="header-menu-panel">
        <div class="container">
            <? endif ?>



            <? endif ?>
            <? endforeach ?>
            <div class="clearfix"></div>
        </div>
    </div>


    <!------------первый ряд работ------------------>


</div>