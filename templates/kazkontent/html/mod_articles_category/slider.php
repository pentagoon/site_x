<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>

<?
JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php'); ?>
<? $language = JFactory::getLanguage()->get('tag'); ?>
<? //$list[0]->jcfields?>

<? $count = 0; ?>


<?php foreach ($list

as $item) : ?>
<? $count++; ?>
<?
$jcFields = FieldsHelper::getFields('com_content.article', $item, true);
$array = json_decode(json_encode($jcFields), True);
?>

<?
foreach ($array as $field):?>
    <?
    //$arr[]=$field;
    //$arr[][$field['name']]['value'] = $field['value'];
    //array_push($fields,$field['name'],$field['value'],$field['rawvalue']);
    $arr[$field['name']] = $field;
    $item->introtext = $arr;
    $arr[$item->id] = $item;

    ?>

<? endforeach ?>


<? if ($count == 1): ?>
<? $img1 = $item->introtext['logotip']['value']; ?>
<? $god = $item->introtext['god-statistiki']['value']; ?>
<? $user = $item->introtext['unikalnykh-polzovatelej']['value']; ?>
<? $view = $item->introtext['prosmotrov']['value']; ?>
<? $seans = $item->introtext['dlitelnost-seansa']['value']; ?>
<div style="visibility: hidden;" class="values-info-container logoimg">
    <div class="container text-center mt-5 logoimg2"
         data-target="<?php echo $img ?>">
        <?= $img1 ?>
    </div>


    <div class="container">
        <ul class="values-panel mt-4">

            <li class="mt-3 unikum">
                <span class="bold-label-700"><?= $user ?></span>
                <div>
                    <? if ($language == 'ru-RU'): ?>уникальных пользователей за <span><?= $god ?></span> год
                    <? elseif ($language == 'kk-KZ'): ?><span><?= $god ?></span> жылғы бірегей пайдаланушылар
                    <? else: ?>уникальных пользователей за <span><?= $god ?></span> год
                    <? endif ?>
                </div>
            </li>
            <li class="mt-3 prosmotr">
                <span class="bold-label-700"><?= $view ?></span>
                <div>
                    <? if ($language == 'ru-RU'): ?>просмотров за <?= $god ?> год
                    <? elseif ($language == 'kk-KZ'): ?><?= $god ?> жылғы қаралымдар
                    <? else: ?>просмотров за <?= $god ?> год
                    <? endif ?>
                </div>
            </li>
            <li class="mt-3 seans">
                <span class="bold-label-700"><?= $seans ?></span>
                <div>
                    <? if ($language == 'ru-RU'): ?>средняя длительность сеанса за <?= $god ?> год
                    <? elseif ($language == 'kk-KZ'): ?><?= $god ?> жылғы сеанстың орташа ұзақтығы
                    <? else: ?>средняя длительность сеанса за <?= $god ?> год
                    <? endif ?>
                </div>
            </li>
        </ul>
    </div>


</div>
<div class="site-list-container">
    <div class="block-title  d-flex align-items-center justify-content-center">
        <? if ($language == 'ru-RU'): ?>САЙТЫ
        <? elseif ($language == 'kk-KZ'): ?>САЙТТАР
        <? else: ?>САЙТТАР
        <? endif ?>
    </div>
    <ul>
        <?php $img = json_decode($item->images); ?>
        <? $img2 = $img ?>
        <? endif ?>














        <? $img1 = $item->introtext['logotip']['value']; ?>
        <? $god = $item->introtext['god-statistiki']['value']; ?>
        <? $user = $item->introtext['unikalnykh-polzovatelej']['value']; ?>
        <? $view = $item->introtext['prosmotrov']['value']; ?>
        <? $seans = $item->introtext['dlitelnost-seansa']['value']; ?>


        <?php $img = json_decode($item->images); ?>
        <? $img2 = $img ?>

        <li>
            <div class="d-flex justify-content-between align-items-center">
                <a class="slider-links"
                   data-target="<?php echo $img2->image_intro; ?>"
                   data-user='<?= $user ?>'
                   data-view='<?= $view ?>'
                   data-seans='<?= $seans ?>'
                   data-god='<?= $god ?>'
                   data-img='<?= $img1 ?>'
                   data-index='<?= $count ?>'
                   href="#">
                    <?php if ($item->params->get('show_title')) : ?>
                        <?php echo $item->title; ?>
                    <?php endif; ?>
                </a>
                <span class="badge  badge-pill">
        <a href="#">
	   
		<img src="<?php echo JUri::base(); ?>templates/kazkontent/images/arrow-to-right.png" class="img-fluid">
	    </a>
                        </span>
            </div>
        </li>
        <? endforeach ?>
    </ul>

</div>

<div class="slider-items">
    <div class="slider-item img-fluid"
         style="background: url('<?php echo JUri::base(); ?>templates/kazkontent/images/slider_1_bg.png');">
        <div id="slider-image-container" class="slider-content d-flex justify-content-center">
            <img class="align-self-end img-fluid centerimg" style="visibility: hidden;"
                 src="<?php echo $img2->image_intro; ?>">
        </div>
    </div>
</div>

 
 
 
 
 


