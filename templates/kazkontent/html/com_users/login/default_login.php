<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

?>

<div class="about-page">

<div class="coment-form">
<div class="c_contacts">



	<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-horizontal">

		
		<div class="control-group">
						<div class="control-label">
							<?php echo JText::_($this->form->getFieldAttribute('username','label'));?>
							<?php if($this->form->getFieldAttribute('username','required')) :?>
								*
							<?php endif;?>
						</div>
						<div class="controls">
							<?php $this->form->setFieldAttribute('username','class','my_c')?>
							<?php echo $this->form->getInput('username'); ?>
						</div>
		</div>
		
		
		<div class="control-group">
						<div class="control-label">
							<?php echo JText::_($this->form->getFieldAttribute('password','label'));?>
							<?php if($this->form->getFieldAttribute('password','required')) :?>
								*
							<?php endif;?>
						</div>
						<div class="controls">
							<?php $this->form->setFieldAttribute('password','class','my_c')?>
							<?php echo $this->form->getInput('password'); ?>
						</div>
		</div>

			<?php $tfa = JPluginHelper::getPlugin('twofactorauth'); ?>

			<?php if (!is_null($tfa) && $tfa != array()): ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getField('secretkey')->label; ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getField('secretkey')->input; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
			<div  class="control-group">
				<label><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?></label>
				<span>&nbsp;<input class="my_check" id="remember" type="checkbox" name="remember" class="inputbox" value="yes"/></span>
			</div>
			<?php endif; ?>

			<div class="controls">
				<button type="submit" class="btn btn-primary">
					<?php echo JText::_('JLOGIN'); ?>
				</button>
			</div>
			<br /><br />

			<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		
	</form>
</div>
</div>

</div>
