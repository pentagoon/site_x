<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_categories
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
$u = JFactory::getUri();

defined('_JEXEC') or die;
?>
<ul class="potfolio-nav">

<?php foreach($list as $item) :?>
 	<li>
		<a href="<?php echo JRoute::_(ContentHelperRoute::getCategoryRoute($item->id));?>"
<?php if($u->getPath() == JRoute::_(ContentHelperRoute::getCategoryRoute($item->id))) :?>
	class="act"
<?php endif;?>		
		
		><?php echo $item->title;?></a>
	</li> 
<?php endforeach; ?>
	
</ul>
