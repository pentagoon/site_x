<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Including fallback code for the placeholder attribute in the search field.
JHtml::_('jquery.framework');
JHtml::_('script', 'system/html5fallback.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

if ($width)
{
	$moduleclass_sfx .= ' ' . 'mod_search' . $module->id;
	$css = 'div.mod_search' . $module->id . ' input[type="search"]{ width:auto; }';
	JFactory::getDocument()->addStyleDeclaration($css);
	$width = ' size="' . $width . '"';
}
else
{
	$width = '';
}
?>
<?$language = JFactory::getLanguage()->get('tag');?>







<div class="b-search">
	<form action="<?php echo JRoute::_('index.php'); ?>" method="post" class="b-search__form" role="search">
        <a class="b-search__close js__search-close" href="#"></a>
        <input  id="mod-search-searchword <?php echo $module->id ?>" maxlength="<?php $maxlength ?>"
                class="b-search__input" placeholder="Поиск" value="" name="searchword" id="SearchForm_query" type="text">
        <button onclick="this.form.searchword.focus();" class="b-search__submit" name="" type="submit"></button>

		<?php
			$output = '';
			$output .= '<div>
			<input name="searchword" id="mod-search-searchword' . $module->id . '" maxlength="' . $maxlength . '"  class="b-search__input" type="search"' . $width;
			$output .= ' placeholder="' . 'новости' . '" />';

			if ($button) :
				if ($imagebutton) :
					$btn_output = '<span class="input-group-btn">
					<button type="submit" alt="' . $button_text . '" class="btn btn-secondary btn-lg" onclick="this.form.searchword.focus();"/>
					<i class="fa fa-search" aria-hidden="true"></i>

					</button>
					</div></div>';
				else :
					$btn_output = ' <button class="button btn btn-primary" onclick="this.form.searchword.focus();">' . $button_text . '</button>';
				endif;

				switch ($button_pos) :
					case 'top' :
						$output = $btn_output . '<br />' . $output;
						break;

					case 'bottom' :
						$output .= '<br />' . $btn_output;
						break;

					case 'right' :
						$output .= $btn_output;
						break;

					case 'left' :
					default :
						$output = $btn_output . $output;
						break;
				endswitch;
			endif;

			//echo $output;
		?>
		<input type="hidden" name="task" value="search" />
		<input type="hidden" name="option" value="com_search" />
		
<?		
$menu = JFactory::getApplication()->getMenu();

$activePage = $menu->getActive()->id;
$defaultPage = $menu->getDefault()->id;
?>		
		
		
			<?if($language == 'ru-RU'):?>
		<input type="hidden" name="Itemid" value="8" />

			<?elseif($language == 'kk-KZ'):?>
		<input type="hidden" name="Itemid" value="22" />

			<?else:?>
		<input type="hidden" name="Itemid" value="8" />

			<?endif?>
		<!---
		<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
		<input type="hidden" name="categories" value="8" />
        <input type="hidden" name="limit" value="1" />
		<----->
	</form>
</div>
<div class="b-search__fade"></div>
