<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_related_items
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div class="latest-progect">
<?php foreach ($list as $item) :	?>
<?php 
$img = json_decode($item->images);
?>
	 <div>
           <a href="<?php echo $item->route;?>">
                 <img src="<?php echo $img->image_intro;?>" alt="<?php echo $img->image_intro_alt;?>" />
                        <h3><?php echo $item->title;?></h3>
                    </a>   
                    <p class="blog-shadow"></p>                                      	
                </div>
<?php endforeach; ?>
</div>
