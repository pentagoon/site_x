<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$cparams = JComponentHelper::getParams('com_media');

jimport('joomla.html.html.bootstrap');

//print_r($this);
?>

<? $language = JFactory::getLanguage()->get('tag'); ?>



    <div class="header-container contact">
        <div class="header-title">
            <div class="d-flex justify-content-center align-self-center align-items-center">
                <?if($language == 'ru-RU'):?>КОНТАКТЫ
                <?elseif($language == 'kk-KZ'):?>БАЙЛАНЫС
                <?else:?>КОНТАКТЫ
                <?endif?>
            </div>
        </div>
        <div class="header-menu">

        </div>
    </div>
<div class="content">
        <div class="section-container mr-5 ml-5">
            <div class="section-body">
                <div class="container mt-4 text-center">
                    <div class="advert-contact mail-link"><a href="mailto:info@kazcontent.kz">info@kazcontent.kz</a>
                    </div>
                    <div class="advert-contact phone">+7 (7172) 79-82-00</div>
                    <div class="advert-contact service-name">
                        <?if($language == 'ru-RU'):?>Республика Казахстан, 010000, г. Нур-Султан, ул. Д. Кунаева, 10, БЦ «Emerald Tower»
                        <?elseif($language == 'kk-KZ'):?>Қазақстан Республикасы, 010000, Нұр-Сұлтан қ., Д. Қонаев к., 10, «Emerald Tower» БО
                        <?else:?>Республика Казахстан, 010000, г. Нур-Султан, ул. Д. Кунаева, 10, БЦ «Emerald Tower»
                        <?endif?>
                        
                    </div>
                    <br>
                    <hr class="mt-4 mb-3">
                </div>


            </div>
            <div class="section-footer">
                <div class="container">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>Редакция ИА «Baq.kz»
                                        <?elseif($language == 'kk-KZ'):?>«Baq.kz» АА Редакциясы
                                        <?else:?>Редакция ИА «Baq.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">+7 (7172) 79-82-32</div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@baq.kz">
                                            mail@baq.kz
                                        </a>
                                        <span>(вн.132)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>Редакция ИА «Bnews.kz»
                                        <?elseif($language == 'kk-KZ'):?>«Bnews.kz» АА редакциясы
                                        <?else:?>Редакция ИА «Bnews.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">+7 (7172) 79-82-22</div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@bnews.kz">
                                            mail@bnews.kz
                                        </a>
                                        <span>(вн.124)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>Редакция ИП  «Adebiportal.kz»
                                        <?elseif($language == 'kk-KZ'):?>«Adebiportal.kz» ИЖ редакция
                                        <?else:?>Редакция ИП  «Adebiportal.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">+7 (7172) 79-82-12</div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@adebiportal.kz">
                                            mail@adebiportal.kz
                                        </a>
                                        <span>(вн.111)</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>
                                            Редакция ИП «E-history.kz»
                                        <?elseif($language == 'kk-KZ'):?>
                                            «E-history.kz» ИЖ редакция
                                        <?else:?>
                                            Редакция ИП «E-history.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">
                                        +7 (7172) 79-82-14
                                    </div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@e-history.kz">
                                            mail@e-history.kz
                                        </a>
                                        <span>(вн.111)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>
                                            ИП «El.kz»
                                        <?elseif($language == 'kk-KZ'):?>
                                            «El.kz» ИЖ
                                        <?else:?>
                                            ИП «El.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">
                                        +7 (7172) 79-82-06
                                    </div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@el.kz">
                                            mail@el.kz
                                        </a>
                                        <span>(вн.158)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>
                                            ИА «Strategy2050.kz»
                                        <?elseif($language == 'kk-KZ'):?>
                                            «Strategy2050.kz» АА
                                        <?else:?>
                                            ИА «Strategy2050.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">
                                        +7 (7172) 79-82-06
                                    </div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@strategy2050.kz">
                                            mail@strategy2050.kz
                                        </a>
                                        <span>(вн.163)</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>
                                            Видео портал «Kaztube.kz»
                                        <?elseif($language == 'kk-KZ'):?>
                                            «Kaztube.kz» бейне порталы
                                        <?else:?>
                                            Видео портал «Kaztube.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">
                                        +7 (7172) 79-82-06
                                    </div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@kaztube.kz">
                                            mail@kaztube.kz
                                        </a>
                                        <span>(вн.157)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>
                                            ИР «Assembly.kz»
                                        <?elseif($language == 'kk-KZ'):?>
                                            «Assembly.kz» ИР
                                        <?else:?>
                                            ИР «Assembly.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">
                                        +7 (7172) 79-82-06
                                    </div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@assembly.kz">
                                            mail@assembly.kz
                                        </a>
                                        <span>(вн.173)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-item-container">
                                    <div class="redaction-name">
                                        <?if($language == 'ru-RU'):?>
                                            ИР «Ruh.kz»
                                        <?elseif($language == 'kk-KZ'):?>
                                            «Ruh.kz» ИР
                                        <?else:?>
                                            ИР «Ruh.kz»
                                        <?endif?>
                                    </div>
                                    <div class="redaction-phone">
                                        +7 (7172) 79-82-06
                                    </div>
                                    <div class="redaction-link">
                                        <a href="mailto:mail@ruh.kz">
                                            mail@ruh.kz
                                        </a>
                                        <span>(вн.145)</span>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <hr class="mt-4 mb-4">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="advert-contact mail-link">
                                    <a href="mailto:sales@kazcontent.kz">
                                        sales@kazcontent.kz
                                    </a>
                                </div>
                                <div class="advert-contact phone">
                                    +7 (7172) 79-82-37
                                </div>
                                <div class="advert-contact service-name f-14">
                                    <?if($language == 'ru-RU'):?>
                                        Коммерческая служба
                                    <?elseif($language == 'kk-KZ'):?>
                                        Коммерциялық қызмет
                                    <?else:?>
                                        Коммерческая служба
                                    <?endif?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="advert-contact mail-link">
                                    <a href="mailto:osvp@kazcontent.kz">
                                        projects@kazcontent.kz
                                    </a>
                                </div>
                                <div class="advert-contact phone">
                                    +7 (7172) 79-82-06
                                </div>
                                <div class="advert-contact service-name f-14">
                                    <?if($language == 'ru-RU'):?>
                                        Отдел сопровождения внешних проектов
                                    <?elseif($language == 'kk-KZ'):?>
                                        Сыртқы жобаларды сүйемелдеу бөлімі
                                    <?else:?>
                                        Отдел сопровождения внешних проектов
                                    <?endif?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="advert-contact mail-link">
                                    <a href="mailto:info@kazcontent.kz">
                                        info@kazcontent.kz
                                    </a>
                                </div>
                                <div class="advert-contact phone">
                                    +7 (7172) 79-82-06
                                </div> 
                                <div class="advert-contact service-name f-14">
                                    <?if($language == 'ru-RU'):?>
                                        Отдел организационно – кадровой работы
                                    <?elseif($language == 'kk-KZ'):?>
                                        Ұйымдастыру-кадр жұмыстары бөлімі
                                    <?else:?>
                                        Отдел организационно – кадровой работы
                                    <?endif?>
                                </div>
                            </div>
                        </div>
                        <hr class="mt-4 mb-0">
                        <br>
                    </div>


                </div>
            </div>
        </div>
    </div>

<?






$db = JFactory::getDbo();
$query = $db->getQuery(true);

    $query->select("contact.misc AS description, contact.image");//какие поля вытаскиваем
    $query->from("#__contact_details AS contact"); //из какой таблицы #__обозначение префикса
    $query->where("contact.published = 1");//те которое опубликрваны
    $query->where("contact.catid = 11");//в ид находится иденфикатор контакта, кто записал статью
    
    $db->setQuery($query);
    $contact = $db->loadAssocList();
?>




<div class="about-page">
    <?php if ($this->params->get('show_email_form') && ($this->contact->email_to || $this->contact->user_id)) : ?>
        <?php  echo $this->loadTemplate('form');  ?>
    <?php endif; ?>
</div>
