<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


if (isset($this->error)) : ?>
	<div class="contact-error">
		<?php echo $this->error; ?>
	</div>
<?php endif; ?>

<div class="comment-form">
<div class="c_contacts">
	<p class="header_contacts"><?php echo JText::_("TPL_SIMPLE_MOBILE_CONTACTS_HEADER");?></p>

	<form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
		
			
			<div class="control-group">
			<?php $this->form->setFieldAttribute('contact_name','class','my_c');?>
			
<?php echo JText::_($this->form->getFieldAttribute('contact_name','label'))?>
<?php if($this->form->getFieldAttribute('contact_name','required')) :?>
	*
<?php endif;?>
				<div class="controls"><?php echo $this->form->getInput('contact_name'); ?></div>
			</div>
			
			
			<div class="control-group">
		<?php $this->form->setFieldAttribute('contact_email','class','my_c');?>			
		<?php echo JText::_($this->form->getFieldAttribute('contact_email','label'))?>
		
		<?php if($this->form->getFieldAttribute('contact_email','required')) :?>
	*
<?php endif;?>			
				
				<div class="controls"><?php echo $this->form->getInput('contact_email'); ?></div>
			</div>
			
			
			<div class="control-group">
	<?php $this->form->setFieldAttribute('contact_message','class','my_ca');?>				
	<?php echo JText::_($this->form->getFieldAttribute('contact_message','label'))?>
	
	<?php if($this->form->getFieldAttribute('contact_message','required')) :?>
	*
<?php endif;?>
				
				<div class="controls"><?php echo $this->form->getInput('contact_message'); ?></div>
			</div>
			
			<?php if ($this->params->get('show_email_copy')) { ?>
				<div class="control-group">
	<?php echo JText::_($this->form->getFieldAttribute('contact_email_copy','label'))?>
	
	<?php $this->form->setFieldAttribute('contact_email_copy','class','my_check');?>			
	<span>&nbsp;<?php echo $this->form->getInput('contact_email_copy'); ?></span>
				</div>
			<?php } ?>
			<?php //Dynamically load any additional fields from plugins. ?>
			<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
				<?php if ($fieldset->name != 'contact'):?>
					<?php $fields = $this->form->getFieldset($fieldset->name);?>
					<?php foreach ($fields as $field) : ?>
						<div class="control-group">
							<?php if ($field->hidden) : ?>
								<div class="controls">
									<?php echo $field->input;?>
								</div>
							<?php else:?>
								<div class="control-label">
									<?php echo $field->label; ?>
									<?php if (!$field->required && $field->type != "Spacer") : ?>
										<span class="optional"><?php echo JText::_('COM_CONTACT_OPTIONAL');?></span>
									<?php endif; ?>
								</div>
								<div class="controls"><?php echo $field->input;?></div>
							<?php endif;?>
						</div>
					<?php endforeach;?>
				<?php endif ?>
			<?php endforeach;?>
			<div class="form-actions"><button class="btn btn-primary validate" type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="task" value="contact.submit" />
				<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
				<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
		
	</form>
</div>	
</div>
