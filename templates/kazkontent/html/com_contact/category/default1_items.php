<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.core');

?>
<div class="content">

    <div class="section-container mr-5 ml-5">
        <div class="section-title mt-5 mb-5">
            <div class="d-flex justify-content-center align-self-center align-items-center">
                Совет Директоров
            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <div class="row">
    <?if($this->items[0]):?>
	<?$count = 0;?>
	<?php foreach ($this->items as $i => $item) : ?>
	<?$count++;?>
	
		
	     
	
			<div class="col-md-4">
                <div class="card text-center border-0 mb-5">
                    <div class="card-body p-0">
                         <img src="<?php echo $item->image;?>" alt="<?php echo $item->name;?>" class="img-fluid"/>
								
                                <p class="card-text mb-0 mt-3 bold-label-700">Председатель Совета директоров</p>
                                <hr class="hr-2">
                                <p class="card-text mb-0 mt-3 label-300">Мауберлинова Нургуль Осербаевна</p>
                                <p class="card-text mb-0 mt-3 label-300">
								<?=$item->misc;?>
								</p>
                            </div>
                        </div>
                    </div>       
	                		<?if($count%3 ===0):?>
		                   <div class='clear'></div>
	                    <?endif?>	
	                
			
			
	<?endforeach?>
	   <?endif?>
		  </div>
			</div>
        </div>
    </div>		
				
				
				





