<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?$language = JFactory::getLanguage()->get('tag');?>
<div class="header-container" >
    <div class="header-title">
        <div class="d-flex justify-content-center align-self-center align-items-center">
			<?if($language == 'ru-RU'):?>
			О КОМПАНИИ
			<?elseif($language == 'kk-KZ'):?>
			КОМПАНИЯ ТУРАЛЫ
			<?else:?>
			О КОМПАНИИ
			<?endif?>
            
        </div>
    </div>
    <div class="header-menu">

    </div>
</div>

<?
$this->subtemplatename = 'items';
echo JLayoutHelper::render('joomla.content.category_default', $this);
