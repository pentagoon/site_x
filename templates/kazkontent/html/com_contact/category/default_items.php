<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.core');

?>
<?$language = JFactory::getLanguage()->get('tag');?>
<div class="content">

    <div class="section-container mr-5 ml-5">
        <div class="section-title mt-5 mb-5">
            <div class="d-flex justify-content-center align-self-center align-items-center">
			<?//$language = JFactory::getLanguage();?>
			
			<?if($language == 'ru-RU'):?>
			Совет Директоров
			<?elseif($language == 'kk-KZ'):?>
			Директорлар кеңесі
			<?else:?>
			Совет Директоров
			<?endif?>
                
            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <div class="row">
    <?if($this->items[0]):?>
	<?$count = 0;?>
	<?php foreach ($this->items as $i => $item) : ?>
	<?$count++;?>
	
		
	     
	
			<div class="col-md-4">
                <div class="card text-center border-0 mb-5">
                    <div class="card-body p-0">
                         <img src="<?php echo $item->image;?>" alt="<?php echo $item->name;?>" class="img-fluid"/>
								
                                <p class="card-text mb-0 mt-3 bold-label-700"><?php echo $item->con_position;?></p>
                                <hr class="hr-2"/>
                                <p class="card-text mb-0 mt-3 label-300"><?php echo $item->name;?></p>
                        <div class="card-text mb-0 p-0 label-300"> <?php echo $item->misc;?></div>
<!--                                <p class="card-text mb-0 mt-3 label-300"></p>-->
                            </div>
                        </div>
                    </div>       
	                		<?if($count%3 ===0):?>
		                   <div class='clear'></div>
	                    <?endif?>	
	                
			
			
	<?endforeach?>
	   <?endif?>
		  </div>
			</div>
        </div>
    </div>		
				
				
			<?
$db = JFactory::getDbo();
$query = $db->getQuery(true);

	$query->select("contact.misc AS description, contact.image,contact.con_position,contact.name");//какие поля вытаскиваем
	$query->from("#__contact_details AS contact"); //из какой таблицы #__обозначение префикса
	$query->where("contact.published = 1");//те которое опубликрваны
	
		if($language == 'ru-RU'){
			$query->where("contact.catid = 20");

		}
		elseif($language == 'kk-KZ'){
			
			$query->where("contact.catid = 26");

		}
		else{
			$query->where("contact.catid = 20");

		}
		
	
	
	$db->setQuery($query);
	$contact = $db->loadAssocList();
?>

<div class="section-container mr-5 ml-5 mb-5">
        <div class="section-title mt-5 mb-5">
            <div class="d-flex justify-content-center align-self-center align-items-center">

    <?if($language == 'ru-RU'):?>
			Руководство
			<?elseif($language == 'kk-KZ'):?>
			Басшылық
			<?else:?>
			Руководство
			<?endif?>


            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <div class="row">
				
				<?foreach($contact as $item):?>
                    <div class="col-md-3">
                        <div class="card text-center border-0 mb-5">
                            <div class="card-body p-0">
                                <img src="<?=$item['image']?>" class="img-fluid">
                                <p class="card-text mb-0 mt-3 bold-label-700">
								<?=$item['con_position']?>
								</p>
                                <hr class="hr-2">
                                <p class="card-text mb-0 mt-3 label-300"><?=$item['name']?></p>
                                <p class="card-text mb-0 mt-3 label-300"> </p>
                            </div>
                        </div>
                    </div>
           
               <?endforeach?>
          
                </div>
            </div>
        </div>
    </div>

	
<?
$db = JFactory::getDbo();
$query = $db->getQuery(true);

	$query->select("contact.misc AS description, contact.image,contact.con_position,contact.name");//какие поля вытаскиваем
	$query->from("#__contact_details AS contact"); //из какой таблицы #__обозначение префикса
	$query->where("contact.published = 1");//те которое опубликрваны
	
		if($language == 'ru-RU'){
				$query->where("contact.catid = 19");//в ид находится иденфикатор контакта, кто записал статью


		}
		elseif($language == 'kk-KZ'){
			
			$query->where("contact.catid = 27");

		}
		else{
			$query->where("contact.catid = 19");

		}
	
	
	$db->setQuery($query);
	$contact = $db->loadAssocList();
?>
 <div class="section-container mr-5 ml-5 mb-5">
        <div class="section-title mt-5 mb-5">
            <div class="d-flex justify-content-center align-self-center align-items-center">
			  <?if($language == 'ru-RU'):?>
			Организационная структура
			<?elseif($language == 'kk-KZ'):?>
			Ұйымдастыру құрылымы
			<?else:?>
		Организационная структура
			<?endif?>
			
			
            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <img src="<?=$contact[0]['image']?>" class="img-fluid">
                <hr>
            </div>
        </div>
    </div>
	
	</div>

