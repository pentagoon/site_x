<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;?>
<?php
// Create a shortcut for params.

$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

///$canEdit = $this->item->params->get('access-edit');

?>
<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
	<div class="system-unpublished">
<?php endif; ?>

<div class="articles-gen-img">
   <?php echo JLayoutHelper::render('joomla.content.intro_image2', $this->item); ?>
   <?php if($params->get('show_publish_date')) :?>
   	<?php echo JLayoutHelper::render('joomla.content.info_block.publish_date2',$this->item);?>
   <?php endif;?>
</div>                

<?php echo JLayoutHelper::render('joomla.content.blog_style_default_item_title2', $this->item); ?>


<?php if (!$params->get('show_intro')) : ?>
	<?php echo $this->item->event->afterDisplayTitle; ?>
<?php endif; ?>
<?php echo $this->item->event->beforeDisplayContent; ?> 
  <?php echo $this->item->introtext; ?>



<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
</div>
<?php endif; ?>

<?php echo $this->item->event->afterDisplayContent; ?>

