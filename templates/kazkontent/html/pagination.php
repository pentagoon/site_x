<?php
function pagination_list_render($list) {
	$html = "";
	
	 $html .= "<ul class='pager'>";
     
	 $html .= $list['start']['data'];
     $html .= $list['previous']['data']; 
	 
	 foreach($list['pages'] as $page) {
	 	$html .= $page['data']; 
	 }
	 
	 $html .= $list['next']['data'];
     $html .= $list['end']['data'];   	
     $html .=  "</ul>";
	return $html;
}
function pagination_item_active($item) {
	
	return "<li><a href='".$item->link."'>".$item->text."</a></li>";
}

function pagination_item_inactive($item) {
	
	if(isset($item->active) && $item->active) {
		return "<li><a class='now'>".$item->text."</a></li>";
	}
	
	return '';
}
?>