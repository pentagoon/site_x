<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>
<?php
// Create a shortcut for params.

$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

///$canEdit = $this->item->params->get('access-edit');

?>
<?$language = JFactory::getLanguage()->get('tag');?>
<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
    || ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00')) : ?>

<?php endif; ?>

<?PHP
$additionalFields = array(); //Создаем пустой массив, имя переменной выставляем по своему усмотрению
foreach ($this->item->jcfields as $field) {/*Пробегаемся циклом по всем полям текущего материала*/
    $additionalFields[$field->name] = $field->value; /*И добавляем их значения в наш ранее созданный массив*/
}
?>

<? //$this->item->cat_id?>
<!--ссылка---->
<?php // echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug,$this->item->cat_id))?>
<?php //echo JLayoutHelper::render('joomla.content.intro_image3',$this->item);?>
<? //php echo $this->item->introtext;?>




<? foreach ($this->items as $item): ?>
    <? foreach ($item->jcfields as $val): ?>
        <? $arr2[$val->name] = $val->value; ?>
    <? endforeach; ?>
    <? $arr[] = $arr2 ?>
<? endforeach ?>

<?php
$db = JFactory::getDbo();
$query = $db->getQuery(true);

$query->select("contact.misc AS description, contact.image,contact.con_position,contact.name");//какие поля вытаскиваем
$query->from("#__contact_details AS contact"); //из какой таблицы #__обозначение префикса
$query->where("contact.published = 1");//те которое опубликрваны

if ($language == 'ru-RU') {
    $query->where("contact.catid = 34");

} elseif ($language == 'kk-KZ') {

    $query->where("contact.catid = 33");

} else {
    $query->where("contact.catid = 34");

}


$db->setQuery($query);
$contactData = $db->loadAssocList();
?>



<div class="content">
    <div class="section-container mr-5 ml-5">
        <div class="section-body">
            <div class="container mt-4 text-center">
                <?php
                $modules = JModuleHelper::getModules("position-8");
                $param = array('style' => "");
                if ($modules) {
                    foreach ($modules as $item) {
                        echo JModuleHelper::renderModule($item, $param);
                    }
                }
                ?>
                <hr class="mt-4 mb-0">
            </div>

            <div class="container mt-4">
                <table class="table marketing-table jobs text-center">
                    <thead class="thead-dark">
                    <tr>
                        <?if($language == 'ru-RU'):?>
                            <th scope="col">ДАТА</th>
                            <th scope="col">ДОЛЖНОСТЬ</th>
                            <th scope="col">ОРГАНИЗАЦИЯ</th>
                            <th scope="col">ДОПОЛНИТЕЛЬНО</th>
                            <th class="p-0"></th>
                        <?elseif($language == 'kk-KZ'):?>
                            <th scope="col">ДАТА</th>
                            <th scope="col">ҚЫЗМЕТІ</th>
                            <th scope="col">ҰЙЫМЫ</th>
                            <th scope="col">ҚОСЫМША</th>
                            <th class="p-0"></th>
                        <?else:?>
                            <th scope="col">ДАТА</th>
                            <th scope="col">ДОЛЖНОСТЬ</th>
                            <th scope="col">ОРГАНИЗАЦИЯ</th>
                            <th scope="col">ДОПОЛНИТЕЛЬНО</th>
                            <th class="p-0"></th>
                        <?endif?>
                    </tr>
                    </thead>
                    <tbody>


                    <? foreach ($arr as $val): ?>
                        <tr>
                            <td><?= $val['data'] ?></td>
                            <td><?= $val['dolzhnost'] ?></td>
                            <td class="p-0">
                                <?= $val['organizatsiya'] ?>
                            </td>
                            <td class=" text-left"> <?= $val['dopolnitelno'] ?></td>
                            <td class="">
                                <div class="accordion-toggle-btn"></div>
                            </td>
                        </tr>
                        <tr class="extra-parameters-block hide">
                            <td class="p-0" colspan="5">
                                <div class="container extra-parameters-container text-left">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="extra-parameters-item">
                                                <div class="parameter-name">
                                                    <?if($language == 'ru-RU'):?>Требования:
                                                    <?elseif($language == 'kk-KZ'):?>Талаптар:
                                                    <?else:?>Требования:
                                                    <?endif?>
                                                </div>
                                                <div class="parameter-separator"></div>
                                                <div class="parameter-content">
                                                    <?= $val['trebovaniya'] ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="extra-parameters-item">
                                                <div class="parameter-name">
                                                    <?if($language == 'ru-RU'):?>Потенциальные обязанности:
                                                    <?elseif($language == 'kk-KZ'):?>Потенциалды міндеттер:
                                                    <?else:?>Потенциальные обязанности:
                                                    <?endif?>
                                                </div>
                                                <div class="parameter-separator"></div>
                                                <div class="parameter-content">
                                                    <?= $val['potentsialnye-obyazannosti'] ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="extra-parameters-item">
                                                <div class="parameter-name">
                                                    <?if($language == 'ru-RU'):?>Условия:
                                                    <?elseif($language == 'kk-KZ'):?>Шарттар:
                                                    <?else:?>Условия:
                                                    <?endif?>
                                                </div>
                                                <div class="parameter-separator"></div>
                                                <div class="parameter-content">
                                                    <?= $val['usloviya'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>

                <hr class="mt-4">
            </div>
        </div>
        <!---------------------------
        <div class="section-footer">
            <div class="container">
                <br>
                
                <div class="next-items-btn">
                    ПОКАЗАТЬ ЕЩЕ
                </div>
                
                <hr>
            </div>
        </div>
        --------------------->
    </div>
    <!--div class="section-container mr-5 ml-5 mb-5">
        <div class="section-title mt-5 mb-5">
            <div class="d-flex justify-content-center align-self-center align-items-center">
                <? if ($language == 'ru-RU'): ?>
                    Команда
                <? elseif ($language == 'kk-KZ'): ?>
                    Команда
                <? else: ?>
                    Команда
                <? endif ?>
            </div>
        </div>
        <div class="section-body">
            <div class="container">
                <div class="row">


                    <?php foreach($contactData as $item):?>
                        <div class="col-md-3">
                            <div class="card text-center border-0 mb-5">
                                <div class="card-body p-0">
                                    <img src="<?=$item['image']?>" class="img-fluid">
                                    <p class="card-text mb-0 mt-3 bold-label-700">
                                        <?=$item['con_position']?>
                                    </p>
                                    <hr class="hr-2">
                                    <p class="card-text mb-0 mt-3 label-300"><?=$item['name']?></p>
                                    <p class="card-text mb-0 mt-3 label-300"> </p>
                                </div>
                            </div>
                        </div>

                    <?php endforeach?>

                </div>
            </div>
        </div>
    </div-->
</div>


<?php echo JLayoutHelper::render('joomla.content.intro_image01', $this->item); ?>
	
	






	

 
                	