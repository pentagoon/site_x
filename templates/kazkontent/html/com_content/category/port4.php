<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('fieldattach', 'components/com_fieldsattach/helpers/fieldattach.php');
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

?>
<?$language = JFactory::getLanguage()->get('tag');?>
<?php// echo fieldattach::getValue(33, 1, $category = false)?>
<?php //echo fieldattach::getFieldValue(33, 1, $category = false)?>
<?php// echo fieldattach::getName(33, 1, $category = false)?>
		 
	 <div class="header-container reports">
        <div class="header-title">
            <div class="d-flex justify-content-center align-self-center align-items-center">
            <?if($language == 'ru-RU'):?>
			ОТЧЕТНОСТЬ
			<?elseif($language == 'kk-KZ'):?>
			ЕСЕП
			<?else:?>
			ОТЧЕТНОСТЬ
			<?endif?>
            </div>
        </div>
        <div class="header-menu">

        </div>
    </div>
		
		
		
<?		
$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query2 = $db->getQuery(true);
$query2->select("*");//какие поля вытаскиваем
$query2->from("#__fieldsattach AS attach2"); //из какой таблицы 
$query2->where("attach2.groupid  = 2");//в ид находится иденфикатор контакта, кто 

$db->setQuery($query2);
$contact2 = $db->loadAssocList();
foreach($contact2 as $it){
$arc[] = $it['id'];
}
$keys= implode(',',$arc);
//$keys
	$query->select("*");//какие поля вытаскиваем
	//$query->from("#__fieldsattach_values AS attach"); //из какой таблицы
$query->from("#__content AS a"); //из какой таблицы	#__обозначение префикса
$query->join('LEFT', '#__fieldsattach_values AS attach ON (a.id = attach.articleid)'); 

	$query->where("attach.fieldsid in({$keys})");


	if($language == 'ru-RU'){
		    $query->where("a.language = 'ru-RU'");

	}
	
	if($language == 'kk-KZ'){
		    $query->where("a.language = 'kk-KZ'");

	}
	
	
	
	$db->setQuery($query);
	$contact = $db->loadAssocList();
	?>

<!---------------масив с файлом-------------------->
<?$flag =false?>
<?
foreach($contact as &$item){
	$item['value'] =substr($item['value'],0,strpos($item['value'],'|'));
$arr[$item['articleid']]['pdf'] = $item['value'];
	if($item['value']){
		$flag = true;
	}
}
?>

<!------добавить кастомное поле и обработать дату-------------------->
<?php foreach ($this->lead_items as $k=>$item) : ?>
   <?foreach($item->jcfields as $it):?>
 
	  <?if($it->name == 'data-otchetnosti'):?>
	  <?//$substr= substr($it->value,-4)?>
	  	 <?preg_match('/(\d){4}/i',$it->value,$arr8)?>

	  <?$it->value = $arr8[0]?>
	 <? //preg_match('/(\d){4}/i',$it->value,$arr)?>
	  
	  <?endif?>
	  
	<?$arr[$item->id][$it->name] = $it->value ?>
	<?if(is_array($it->rawvalue)):?>
	<?$arr[$item->id]['tip']=$it->rawvalue[0] ?>
	<?endif?>
	<?$arr[$item->id]['stati'] =  $item->id?>
	
   <?endforeach?>
   <?php endforeach; ?>

<!--делаем сортировку-->		
<?foreach($arr as $k=>$item1){
	if($item1['tip']){
		$arr2[$item1['tip']][$item1['data-otchetnosti']][]=$item1;
	}
	
	}
?>


						
<!--формируем масив типов, чтобы вывести заголовки по ид-->
<?foreach($arr as $item3):?>
<?$arr3[$item3['tip']] = $item3['vybrat-tip-otchetnosti'];?>
<?endforeach?>		
		
		
		
		
		
		
<?//$image= $this->lead_items[0]->images->image_intro;?>
<?//$img=json_decode($image);?>
<?//$decod= json_decode($item->images)->image_intro?>
<?//$image= $this->lead_items[0]->id;?>



    <div class="content">
        <div class="section-container mr-5 ml-5">
            <div class="section-body">

                <hr class="mt-3 mb-4">
                <div class="messages-container mb-5">
                    <div class="container">
                    	<?if($language == 'ru-RU'):?>Конкурсы АО «Казконтент» по государственным закупкам товаров, работ и услуг осуществляется в
                        соответствии с законом Республики Казахстан «О государственных закупках».
		                <?elseif($language == 'kk-KZ'):?> «Қазконтент» АҚ жасаланатын бүкіл мемлекеттік сатып алу конкурстары, Қазақстан Республикасының «Мемлекеттік сатып алулар туралы» заңымен сәйкес өткізіледі. 
		                <?else:?>Конкурсы АО «Казконтент» по государственным закупкам товаров, работ и услуг осуществляется в
                        соответствии с законом Республики Казахстан «О государственных закупках».
		                <?endif?>
                    </div>
                </div>

                <div class="container">
				<?foreach($arr2 as $key=>$item2):?>
				
				              <div class="card border-0">
                        <div class="card-header text-center py-2 bg-transparent">
                            <h5 class="mb-0">
                                <div class="label-300">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div></div>
				                            <?if($language == 'ru-RU'):?><?=$arr3[$key]?>
				                            <?elseif($language == 'kk-KZ'):?>
				                            	<?if($arr3[$key] == 'ФИНАНСОВЫЕ ОТЧЕТЫ'):?>ҚАРЖЫЛЫҚ ЕСЕПТЕР
				                            	<?elseif($arr3[$key] == 'ГОСУДАРСТВЕННЫЕ ЗАКУПКИ '):?>МЕМЛЕКЕТТІК САТЫП АЛУЛАР
				                            	<?else:?><?=$arr3[$key]?>
				                            	<?endif?>
				                            <?else:?><?=$arr3[$key]?>
				                            <?endif?>
										<span class="badge"><div mode="reports"
                                                                                         class="accordion-toggle-btn"></div></span>
                                    </div>
                                </div>
                            </h5>
                        </div>

                        <div class="collapse">
                            <div class="card-body extra-parameters-container">
                                <div class="row">
                          <?foreach($item2 as $key2=>$item3):?>
						       <div class="col-md-6">
                                        <div class="extra-parameters-item">
                                            <div class="parameter-name">
											<?=$key2?></div>
                                            <div class="parameter-separator"></div>
                                            <div class="parameter-content">
							<?foreach($item3 as $item4):?>
                                  <div class="download-link">
								<a href="/images/documents/<?=$item4['stati']?>/<?=$item4['pdf']?>">
								<?=$item4['zagolovok-zagruzhennogo-fajla']?>
												</a></div>
												<?endforeach?>
													
											</div>
                                        </div>
                                    </div>
						  <?endforeach?>
                               
                           
									
							
									
									
									
									
									
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				
				
				
				
				<?endforeach?>
				
      


   

                </div>
            </div>
        </div>

 
    </div>

