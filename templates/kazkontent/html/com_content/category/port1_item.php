<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;?>
<?php
// Create a shortcut for params.

$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

///$canEdit = $this->item->params->get('access-edit');

?>
<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
	<div class="system-unpublished">
<?php endif; ?>

<div class="portfolio-block-img">
    <?php echo JLayoutHelper::render('joomla.content.intro_image3',$this->item);?>                                                     	
</div>
 <div class="portfolio-block-txt">
                	<?php if($params->get('show_title')) :?>
					<h2>
						<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug,$this->item->cat_id))?>">
							<?php echo $this->item->title?>
						</a>
					</h2>
					<?php echo $this->item->event->afterDisplayTitle; ?>
					<?php endif;?>
					
                    <p><?php echo $this->item->introtext;?> </p>
</div>


<?php echo $this->item->event->beforeDisplayContent; ?> 




<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
</div>
<?php endif; ?>

<?php echo $this->item->event->afterDisplayContent; ?>

