<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);



$info    = $params->get('info_block_position', 0);
$template = JFactory::getApplication()->getTemplate();
if(!empty($this->item->contactid)) {
	$db = JFactory::getDbo();
$query = $db->getQuery(true);

	$query->select("contact.misc AS description, contact.image");
	$query->from("#__contact_details AS contact");
	$query->where("contact.published = 1");
	$query->where("contact.id = ".$this->item->contactid);
	
	$db->setQuery($query);
	$contact = $db->loadAssoc();
}


?>
<div class="content">
	<div class="post-single">
		<div class="post-single-head">
			<?php if($params->get('show_publish_date')) :?>
			<p class="date">
				<span>
					<?php echo JHtml::_('date',$this->item->publish_up,'d');?>
				</span>
			<?php echo JHtml::_('date',$this->item->publish_up,'M');?></p>
			<?php endif;?>
			
	         <?php if($params->get('show_title')) :?> 
			 	<h1>
			 		<?php echo $this->escape($this->item->title);?>
				</h1>	
			  <?php endif;?>
	                
	         <p class="post-single-meta">
			  <?php if($params->get('show_author')) :?> 
			  	<img src="<?php echo JUri::base().'templates/'.$template;?>/images/ico-admin-post.jpg" alt="" />
				<?php if($params->get('link_author') && !empty($this->item->contact_link)) :?>
	<?php echo JHtml::_('link',$this->item->contact_link,$this->item->author); ?> 
				 <?php endif;?>
					
			  <?php endif;?>
			  <?php if($params->get('show_tags') && !empty($this->item->tags)) :?> 
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <img src="<?php echo JUri::base().'templates/'.$template;?>/images/ico-metki-post.jpg" alt="" />
			  <?php echo JLayoutHelper::render('joomla.content.mytags',$this->item->tags->itemTags)?>
			  <?php endif;?>	
			</p>
	    </div>
		
		<?php echo $this->item->event->beforeDisplayContent;?>
		
		<?php if(!empty($images->image_fulltext)) :?>
			<?php if(!empty($images->image_fulltext_captiont)) :?>
				<?php $caption = htmlspecialchars($images->image_fulltext_captiont);?>
			<?php endif;?>
			<img title="<?php echo $caption;?>" alt="" src="<?php echo $images->image_fulltext;?>">
		<?php endif;?>
		
		<?php echo $this->item->text;?>	
		
	</div>
	<?php if(!empty($contact)) :?>
	<div class="author-main">
            <h2><?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY',$this->item->author);?></h2>
                <div class="author">
                	<img src="<?php echo $contact['image'];?>">
                    <?php if(!empty($contact['description'])) :?>
						<p>
							<?php echo $contact['description'];?>
						</p>
					<?php endif; ?>
                </div>
            </div>
	<?php endif;?>
	 
			
		<?php
			$modules = JModuleHelper::getModules("position-10");
			$param = array('style' => "simple_mobile_related");
			
			foreach($modules as $item) {
				echo JModuleHelper::renderModule($item,$param);
			}
		?>	
			
			
	<?php echo $this->item->event->afterDisplayContent; ?>
</div>	
			
	