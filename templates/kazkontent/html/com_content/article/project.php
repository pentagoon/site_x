<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params = $this->item->params;
$images = json_decode($this->item->images);
$urls = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user = JFactory::getUser();
$info = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
$useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
    || $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author'));

?>
<? $language = JFactory::getLanguage()->get('tag'); ?>

<div class="header-container project">
    <div class="header-title">
        <div class="d-flex justify-content-center align-self-center align-items-center">
            <? if ($language == 'ru-RU'): ?>
                НАШИ ПРОЕКТЫ
            <? elseif ($language == 'kk-KZ'): ?>
                БІЗДІҢ ЖОБАЛАР
            <? else: ?>
                НАШИ ПРОЕКТЫ
            <? endif ?>
        </div>
    </div>


    <?php
    $modules = JModuleHelper::getModules("position-9");
    $param = array('style' => "");
    if ($modules) {
        foreach ($modules as $item) {
            echo JModuleHelper::renderModule($item, $param);
        }
    }
    ?>


    <?php
    $modules = JModuleHelper::getModules("position-4");
    $param = array('style' => "");
    if ($modules) {
        foreach ($modules as $item) {
            echo JModuleHelper::renderModule($item, $param);
        }
    }
    ?>

</div>

<? foreach ($this->item->jcfields as $k => $item) {
    //$arr[$item->group_id][$item->params->get('render_class')][$item->name][]=$item->title;
    //$arr[$item->group_id][$item->params->get('render_class')][$item->name][]=$item->name;

    if ($item->value) {
        $arr[$item->group_id][$item->params->get('render_class')][$item->name] = $item->value;
    }


} ?>


<div class="content">
    <div class="section-container mr-5 mt-5 mb-5 ml-5">
        <div class="section-body">

            <div class="container">
                <div class="row">
                    <div class="col-md-4">

                        <?php if (!empty($images->image_fulltext)) : ?>
                            <?php if (!empty($images->image_fulltext_captiont)) : ?>
                                <?php $caption = htmlspecialchars($images->image_fulltext_captiont); ?>
                            <?php endif; ?>
                            <img title="<?php echo $caption; ?>" alt="" src="<?php echo $images->image_fulltext; ?>"
                                 class="img-fluid">
                        <? else: ?>
                            изображения нет
                        <?php endif; ?>


                        <div class="text-right social-icons-container">
                            <? foreach ($arr[3] as $kk => $item): ?>

                                <? $v = substr($kk, -1); ?>


                                <a target="_blank" href="<?= $item['social-' . $v] ?>">
                                    <?= $item['izobrazhenie-' . $v] ?>
                                </a>

                            <? endforeach ?>


                            <div class="clear-fix"></div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="bold-label-700 f-15"><?= $arr[4]['POLE']['pervyj-abzats'] ?>
                        </div>
                        <div class="border-hr-70"></div>
                        <div class="text-uppercase label-300 f-14">
                            <? if ($arr[4]['POLE']['vtoroj-abzats']): ?>
                                <?= $arr[4]['POLE']['vtoroj-abzats'] ?>
                            <? else: ?>
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbspНет даных для вывода
                            <? endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-footer">

        </div>
    </div>
    <? $flag = false ?>
    <? foreach ($arr[2] as $it): ?>
        <? foreach ($it as $it2): ?>
            <? if ($it2): ?>
                <? $flag = TRUE;
                break; ?>

            <? endif ?>
        <? endforeach ?>
    <? endforeach ?>

    <? if ($flag): ?>

        <div class="section-container mr-5 ml-5 mb-5">
            <div class="section-title mt-5 mb-5">
                <div class="d-flex justify-content-center align-self-center align-items-center">
                    <? if ($language == 'ru-RU'): ?>
                        Команда
                    <? elseif ($language == 'kk-KZ'): ?>
                        Команда
                    <? else: ?>
                        Команда
                    <? endif ?>
                </div>
            </div>
            <div class="section-body">
                <div class="container">
                    <div class="row">

                        <? foreach ($arr[2] as $k => $item): ?>

                            <div class="col-md-3">
                                <div class="card text-center border-0 mb-5">
                                    <div class="card-body p-0">
                                        <? //$v=substr($k,-1);?>
                                        <? $v = substr($k, 5); ?>
                                        <?= $item['kartinka-sotrudnik-' . $v] ?>
                                        <p class="card-text mb-0 mt-3 bold-label-700">
                                            <?= $item['dolzhnost-sotrudnika-' . $v] ?></p>
                                        <hr class="hr-2">
                                        <p class="card-text mb-0 mt-3 bold-label-700">
                                            <?= $item['imya-sotrudnika-' . $v] ?>

                                        </p>


                                    </div>
                                </div>
                            </div>


                        <? endforeach ?>


                    </div>

                    <hr class="mt-5 mb-5">
                </div>
            </div>
        </div>
    <? endif ?>
</div>
	
		
	