<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$language = JFactory::getLanguage()->get('tag');
// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
$useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
	|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author'));

?>
  <div class="header-container project">
        <div class="header-title">
            <div class="d-flex justify-content-center align-self-center align-items-center">
               <?if($language == 'ru-RU'):?>
                	НОВОСТИ
	            <?elseif($language == 'kk-KZ'):?>
	                ЖАҢАЛЫҚТАР
	            <?else:?>
	                НОВОСТИ
	            <?endif?>
            </div>
        </div>
	
	
	
	
	
	    
		
	</div>
	

	
	
	
	

    <div class="content">
        <div class="section-container mr-5 mt-5 mb-5 ml-5">
            <div class="section-body">

                <div class="container">
                    <div class="row">
                      <div class="col-md-4">
					  
			<?php if(!empty($images->image_fulltext)) :?>
			<?php if(!empty($images->image_fulltext_captiont)) :?>
				<?php $caption = htmlspecialchars($images->image_fulltext_captiont);?>
			<?php endif;?>
			<img title="<?php echo $caption;?>" alt="" src="<?php echo $images->image_fulltext;?>" class="img-fluid">
			<?else:?>
			изображения нет
		<?php endif;?>
					  
					  
					  
					  
                           

                      
                        </div>
						
						
						
						
						
						
						
						
                        <div class="col-md-8">
                            <div class="bold-label-700 f-15">
							
								<?php if ($params->get('show_title')) : ?>
				<?php if ($params->get('link_titles') && !empty($this->item->readmore_link)) : ?>
					<?php echo $this->escape($this->item->title); ?>
				<?php else : ?>
					<?php echo $this->escape($this->item->title); ?>
				<?php endif; ?>
			<?php endif; ?>
							
                            </div>
                            <div class="border-hr-70"></div>
                            <div class="text-uppercase label-300 f-14">
						<?php echo $this->item->text;?>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-footer">

            </div>
        </div>



    </div>
	
		
	