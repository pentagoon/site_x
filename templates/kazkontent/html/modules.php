<?php

defined('_JEXEC') or die;

function modChrome_style1($module, &$params, $attribs) {
	if($module->content) {
		echo $module->content;
	}
}

function modChrome_style2($module, &$params) {
	?>
	    <div class="section-container mr-5 ml-5">
        <div class="section-title mt-5 mb-5">
            <div class="d-flex justify-content-center align-self-center align-items-center text-center">
                <div class="w-75 company-description-label"> 
<?
	
	if($module->content) {
		echo $module->content;
	}
	?>
	         </div>
            </div>
        </div>
        <div class="section-body">

        </div>
    </div>
	<?
}


function modChrome_style3($module, &$params) {
	
	if($module->showtitle) {
		$headerlevel = $params->get('header_tag') ? $params->get('header_tag') : 'h1';
		echo "<".$headerlevel.">".$module->title."</".$headerlevel.">";
	}
	echo " <div class='latest-progect'>";
	
	if($module->content) {
		echo $module->content;
	}
	echo "</div>";
}

function modChrome_style4($module, &$params) {
	
	if($module->showtitle) {
		$headerlevel = $params->get('header_tag') ? $params->get('header_tag') : 'h1';
		echo "<".$headerlevel.">".$module->title."</".$headerlevel.">";
	}
	echo "<div class='from-blog'>";
	
	if($module->content) {
		echo $module->content;
	}
	echo "</div>";
}

function modChrome_style5($module, &$params) {
	
	echo "<div class='widget'>";
	
	if($module->showtitle) {
		$headerlevel = $params->get('header_tag') ? $params->get('header_tag') : 'h1';
		echo "<".$headerlevel.">".$module->title."</".$headerlevel.">";
	}
	
	if($module->content) {
		echo $module->content;
	}
	
	echo "</div>";
}

function modChrome_style6($module, &$params) {
	
	echo "<div class='related-post-main'>";
	
	if($module->showtitle) {
		$headerlevel = $params->get('header_tag') ? $params->get('header_tag') : 'h1';
		echo "<".$headerlevel.">".$module->title."</".$headerlevel.">";
	}
	
	if($module->content) {
		echo $module->content;
	}
	
	echo "</div>";
}

function modChrome_style7($module, &$params) {
	
	echo "<div class='latest-progect-main'>";
	
	if($module->showtitle) {
		$headerlevel = $params->get('header_tag') ? $params->get('header_tag') : 'h1';
		echo "<".$headerlevel.">".$module->title."</".$headerlevel.">";
	}
	
	if($module->content) {
		echo $module->content;
	}
	
	echo "</div>";
}