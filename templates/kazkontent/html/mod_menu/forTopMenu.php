<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$menu = JFactory::getApplication()->getMenu();

// Note. It is important to remove spaces between elements.
?>
<?php // The menu class is deprecated. Use nav instead. ?>
<ul>
<?php
foreach ($list as $i => &$item)
{
	
	$current = FALSE;

	if ($item->id == $menu->getActive()->id)
	{
		$current = TRUE;
	}


	echo '<li>';

		if($current) {
			echo "<a class='now' href='".$item->flink."'>".$item->title."</a>";
		}
		else {
			echo "<a href='".$item->flink."'>".$item->title."</a>";
		}
	
	echo '</li>';
}
?>
	
</ul>
