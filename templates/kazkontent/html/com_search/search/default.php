<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('formbehavior.chosen', 'select');

?>
<? $cat = JUri::getInstance()->getVar('Itemid'); ?>

<div class="search-content">

    <div class="section-container">
        <div class="section-body company-news-widget-container">
            <div class="container">
                <?php echo $this->loadTemplate('form'); ?>
                <?php if ($this->error == null) : ?>

                    <div class="row">

                        <div class='clear'></div>
                        <?php echo $this->loadTemplate('results'); ?>
                    </div>
                <? endif ?>

            </div>
        </div>
    </div>

<!--    --><?// if ($cat == 13): ?>
<!--        <div class="section-container">-->
<!--            <div class="section-body company-news-widget-container">-->
<!--                <div class="container">-->
<!---->
<!--                    --><?php //echo $this->loadTemplate('form'); ?>
<!---->
<!---->
<!--                    <div class="row">-->
<!--                        <div class='clear'></div>-->
<!--                        --><?php //echo $this->loadTemplate('results'); ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    --><?// endif ?>
<!---->
<!--    --><?// if ($cat == 8 || $cat == 22): ?>
<!--        -->
<!---->
<!--    --><?php //else : ?>
<!--        --><?php //echo $this->loadTemplate('error'); ?>
<!--    --><?php //endif; ?>
</div>