<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();

?>
<? $language = JFactory::getLanguage()->get('tag'); ?>

<div class="b-search">

    <form class="b-search__form" id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search'); ?>"
          method="get">
<!--        <input type="hidden" name="task" value="search"/>-->
        <a class="b-search__close js__search-close" href="#"></a>
        <input class="b-search__input" placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>"
               value="<?php echo $this->escape($this->origkeyword); ?>"
               title="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" name="searchword" id="search-searchword"
               size="30" maxlength="<?php echo $upper_limit; ?>" type="text">
        <button class="b-search__submit" name="" type="submit"></button>
    </form>
</div>
<!--<div class="filter-pane">-->
<!--    <div class="filter-pane__inner">-->
<!--        <span class="filter-pane__title">CОРТИРОВАТЬ</span>-->
<!--        <div class="filter-pane__items">-->
<!--            <a class="item active" href="#" data-filter="sort,rel">По релевантности</a>-->
<!--            <a class="item" href="#" data-filter="sort,date">По дате</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->




