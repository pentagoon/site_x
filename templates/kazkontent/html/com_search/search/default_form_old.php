<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();

?>
<? $language = JFactory::getLanguage()->get('tag'); ?>

<div class="b-search">

    <form class="b-search__form" id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search'); ?>"
          method="get">
        <input type="hidden" name="task" value="search"/>
        <a class="b-search__close js__search-close" href="#"></a>
        <input class="b-search__input" placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>"
               value="<?php echo $this->escape($this->origkeyword); ?>"
               title="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" name="searchword" id="search-searchword"
               size="30" maxlength="<?php echo $upper_limit; ?>" type="text">
        <button class="b-search__submit" name="" type="submit"></button>
    </form>
</div>
<div class="filter-pane">
    <div class="filter-pane__inner">
        <span class="filter-pane__title">Ограничение области поиска</span>
        <div class="filter-pane__items">
            <a class="item active" href="#" data-filter="sort,rel">Работа</a>
            <a class="item" href="#" data-filter="sort,date">Новости</a>
        </div>
    </div>
</div>

<div class="search-result">
    <div class="search-result__summary">
        <?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span>' . $this->total . '</span>'); ?>
    </div>
    <ul class="search-result__list">
        <li class="search-result__item">
            <a class="search-link"
               href="#">
                <span class="article-title">Телемост с Джеймсом Холлисом «Раненый целитель» при поддержке <span
                        class="highlight">журнала</span> PSYCHOLOGIES</span>
                <div class="article-content">
                    Сегодня, 14 июня, в 19:00 в Московском институте психоанализа состоится трехчасовой телемост с
                    Джеймсом Холлисом, юнгианским аналитиком и одним из самых известных современных мировых экспертов в
                    ...
                </div>
                <div class="article-content">
                    Тема встречи – «Раненый целитель», архетип специалиста помогающих профессий. Как заметил Юнг, только
                    раненый целитель исцеляет. Только человек, который пришел через страдания к более глубокому ...
                </div>
                <div class="search-result__item_info">
                    <span class="date">15.06.2017</span>
                    <span class="path">
                                        пресс-центр                                         / PSYCHOLOGIES                                                                                                                    </span>
                </div>
            </a>
        </li>
        <li class="search-result__item">
            <a class="search-link"
               href="#">
                <span class="article-title">Телемост с Джеймсом Холлисом «Раненый целитель» при поддержке <span
                        class="highlight">журнала</span> PSYCHOLOGIES</span>
                <div class="article-content">
                    Сегодня, 14 июня, в 19:00 в Московском институте психоанализа состоится трехчасовой телемост с
                    Джеймсом Холлисом, юнгианским аналитиком и одним из самых известных современных мировых экспертов в
                    ...
                </div>
                <div class="article-content">
                    Тема встречи – «Раненый целитель», архетип специалиста помогающих профессий. Как заметил Юнг, только
                    раненый целитель исцеляет. Только человек, который пришел через страдания к более глубокому ...
                </div>
                <div class="search-result__item_info">
                    <span class="date">15.06.2017</span>
                    <span class="path">
                                        пресс-центр                                         / PSYCHOLOGIES                                                                                                                    </span>
                </div>
            </a>
        </li>
        <li class="search-result__item">
            <a class="search-link"
               href="#">
                <span class="article-title">Телемост с Джеймсом Холлисом «Раненый целитель» при поддержке <span
                        class="highlight">журнала</span> PSYCHOLOGIES</span>
                <div class="article-content">
                    Сегодня, 14 июня, в 19:00 в Московском институте психоанализа состоится трехчасовой телемост с
                    Джеймсом Холлисом, юнгианским аналитиком и одним из самых известных современных мировых экспертов в
                    ...
                </div>
                <div class="article-content">
                    Тема встречи – «Раненый целитель», архетип специалиста помогающих профессий. Как заметил Юнг, только
                    раненый целитель исцеляет. Только человек, который пришел через страдания к более глубокому ...
                </div>
                <div class="search-result__item_info">
                    <span class="date">15.06.2017</span>
                    <span class="path">
                                        пресс-центр                                         / PSYCHOLOGIES                                                                                                                    </span>
                </div>
            </a>
        </li>
    </ul>
    <div class="btn-holder">
        <a class="btn js-result-more" href="#">показать еще</a>
    </div>
</div>


<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search'); ?>" method="post">
    <!--    <div class="b-search">-->
    <!--        <form class="b-search__form" id="search-form" action="/search/" method="GET">    <a class="b-search__close js__search-close" href="#"></a>-->
    <!--            <input class="b-search__input" name="searchword" title="-->
    <?php //echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?><!--" placeholder="-->
    <?php //echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?><!--" id="search-searchword" size="30" maxlength="-->
    <?php //echo $upper_limit; ?><!--" value="--><?php //echo $this->escape($this->origkeyword); ?><!--" type="text">-->
    <!--            <button class="b-search__submit" name="" type="submit"></button>-->
    <!--        </form>-->
    <!--    </div>-->
    <div class="btn-toolbar">
        <div class="btn-group pull-left">
            <input type="text" name="searchword" title="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>"
                   placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" id="search-searchword" size="30"
                   maxlength="<?php echo $upper_limit; ?>" value="<?php echo $this->escape($this->origkeyword); ?>"
                   class="inputbox"/>
        </div>
        <div class="btn-group pull-left">
            <button name="Search" onclick="this.form.submit()" class="btn hasTooltip"
                    title="<?php echo JHtml::_('tooltipText', 'COM_SEARCH_SEARCH'); ?>">
                <span class="icon-search"></span>
                <?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
            </button>
        </div>
        <input type="hidden" name="task" value="search"/>
        <div class="clearfix"></div>
    </div>
    <div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
        <?php if (!empty($this->searchword)) : ?>
            <p>
                <?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span class="badge badge-info">' . $this->total . '</span>'); ?>
            </p>
        <?php endif; ?>
    </div>
    <?php if ($this->params->get('search_phrases', 1)) : ?>
        <fieldset class="phrases">
            <legend>
                <?php echo JText::_('COM_SEARCH_FOR'); ?>
            </legend>
            <div class="phrases-box">
                <?php echo $this->lists['searchphrase']; ?>
            </div>
            <div class="ordering-box">
                <label for="ordering" class="ordering">
                    <?php echo JText::_('COM_SEARCH_ORDERING'); ?>
                </label>
                <?php echo $this->lists['ordering']; ?>
            </div>
        </fieldset>
    <?php endif; ?>
    <?php if ($this->params->get('search_areas', 1)) : ?>
        <fieldset class="only">
            <legend>
                <?php echo JText::_('COM_SEARCH_SEARCH_ONLY'); ?>
            </legend>
            <? $cat = JUri::getInstance()->getVar('Itemid'); ?>


            <? if ($language == 'ru-RU'): ?>

                <label for="area-<?php echo $val; ?>" class="checkbox">
                    работа

                    <input type="radio" name="Itemid"
                           value="13" <?= $cat == 13 ? 'checked' : '' ?>
                           id="area-<?php ; ?>"
                    />
                    <?php echo JText::_($txt); ?>
                </label>


                <label for="area-<?php echo $val; ?>" class="checkbox">
                    новости
                    <input type="radio" name="Itemid"
                           value="8"
                           id="area-<?php ; ?>"
                        <?= $cat == 8 ? 'checked' : '' ?>
                    />
                    <?php echo JText::_($txt); ?>
                </label>

            <? endif ?>


            <? if ($language == 'kk-KZ'): ?>

                <label for="area-<?php echo $val; ?>" class="checkbox">
                    работа

                    <input type="radio" name="Itemid"
                           value="29" <?= $cat == 13 ? 'checked' : '' ?>
                           id="area-<?php ; ?>"
                    />
                    <?php echo JText::_($txt); ?>
                </label>


                <label for="area-<?php echo $val; ?>" class="checkbox">
                    новости
                    <input type="radio" name="Itemid"
                           value="22"
                           id="area-<?php ; ?>"
                        <?= $cat == 8 ? 'checked' : '' ?>
                    />
                    <?php echo JText::_($txt); ?>
                </label>

            <? endif ?>


        </fieldset>
    <?php endif; ?>
    <?php if ($this->total > 0) : ?>
        <div class="form-limit">
            <label for="limit">
                <pre><? //print_r($this->pagination)?></pre>
                <?php // echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>
            </label>
            <?php //echo $this->pagination->getLimitBox(); ?>
        </div>
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
    <?php endif; ?>


</form>
