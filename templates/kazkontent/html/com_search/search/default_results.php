<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$language = JFactory::getLanguage()->get('tag');

?>
<? $cat = JUri::getInstance()->getVar('Itemid'); ?>


<div class="search-result">
    <div class="search-result__summary">
        <?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span>' . $this->total . '</span>'); ?>
    </div>
    <ul class="search-result__list">

        <?php foreach ($this->results as $result) : ?>



            <?php


            $linkArticle = $result->href;

            if ($language === "ru-RU") {
                $linkArticle = "/ru".$result->href;
            }

            ?>

            <li class="search-result__item">
                <a class="search-link"
                   href="<?php echo $linkArticle; ?>"
                    <?php if ($result->browsernav == 1) : ?> target="_blank"<?php endif; ?>>
                    <span class="article-title"><?= $result->title; ?></span>
                    <div class="article-content">
                        <?=$result->text?>
                    </div>
                    <div class="search-result__item_info">
<!--                        <span class="date">-->
<!--                            --><?//=JHtml::_('date', $item->created, 'd.m.Y');?>
<!--<!--                            --><?////= JHtml::_('date', $item->publish_up, 'd.m.Y') ?>
<!--                        </span>-->
                        <span class="path"><?= $result->section ?></span>
                    </div>
                </a>
            </li>
        <?php endforeach; ?>


    </ul>
    <!--    <div class="btn-holder">-->
    <!--        <a class="btn js-result-more" href="#">показать еще</a>-->
    <!--    </div>-->
</div>


<? //if($this->results[0]->jcfields && $cat==13):?>
<!---->
<!--  <div class="section-body">-->
<!--    <div class="container">-->
<!--      <table class="table marketing-table jobs text-center">-->
<!--        <thead class="thead-dark">-->
<!--          <tr>-->
<!--            <th scope="col">ДАТА</th>-->
<!--            <th scope="col">ДОЛЖНОСТЬ</th>-->
<!--            <th scope="col">ОРГАНИЗАЦИЯ</th>-->
<!--            <th scope="col">ДОПОЛНИТЕЛЬНО</th>-->
<!--            <th class="p-0"></th>-->
<!--          </tr>-->
<!--        </thead>-->
<!--         <tbody>-->
<?php //foreach ($this->results as $result) : ?>
<!--  --><?php
//     $jcFields = FieldsHelper::getFields('com_content.article', $result, true);
//     $array = json_decode(json_encode($jcFields), True);
//   ?>
<!--   --><? //foreach ($array as $field):?>
<!--    --><?php //$arr2[$field['name']]= $field['value'];?>
<!--	--><? //endforeach?><!--	-->
<!--    <tr><td>--><? //=$arr2['data']?><!--</td>-->
<!--      <td>--><? //=$arr2['dolzhnost']?><!--</td>-->
<!--       <td class="p-0">--><? //=$arr2['organizatsiya']?><!--</td>-->
<!--       <td class=" text-left">--><? //=$arr2['dopolnitelno']?><!--</td>-->
<!--       <td class=""><div class="accordion-toggle-btn"></div></td>-->
<!--    </tr>-->
<!--    <tr class="extra-parameters-block hide">-->
<!--       <td class="p-0" colspan="5">-->
<!--          <div class="container extra-parameters-container text-left">-->
<!--             <div class="row">-->
<!--               <div class="col-md-4">-->
<!--                 <div class="extra-parameters-item">-->
<!--                   <div class="parameter-name">Требования:</div>-->
<!--                     <div class="parameter-separator"></div>-->
<!--                      <div class="parameter-content">--><? //=$arr2['trebovaniya']?><!--</div>-->
<!--                       </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-4">-->
<!--                          <div class="extra-parameters-item">-->
<!--                           <div class="parameter-name">Потенциальные обязанности:</div>-->
<!--                            <div class="parameter-separator"></div>-->
<!--                              <div class="parameter-content">-->
<!--                                 --><? //=$arr2['potentsialnye-obyazannosti']?>
<!--                               </div>-->
<!--                            </div>-->
<!--                           </div>-->
<!--                           <div class="col-md-4">-->
<!--                             <div class="extra-parameters-item">-->
<!--                               <div class="parameter-name">Условия:</div>-->
<!--                                  <div class="parameter-separator"></div>-->
<!--                                    <div class="parameter-content">-->
<!--                                    --><? //=$arr2['usloviya']?>
<!--									</div>-->
<!--                               </div>-->
<!--                              </div>-->
<!--                             </div>-->
<!--                            </div>-->
<!--                           </td>-->
<!--                        </tr>-->
<!--	        --><? //endforeach?>
<!--		 </tbody>-->
<!--      </table>-->
<!--	</div>-->
<!--   </div>-->
<!-- -->
<!---->
<!---->
<? //endif?>
<!---->
<!---->
<!---->
<!---->
<!--	-->
<!--	-->
<? //if($cat ==8 || $cat ==22):?>
<!-- --><?php //foreach ($this->results as $result) : ?>
<!--  <div class="col-md-3 p-0">-->
<!--	 --><?php //
//	 if ($result->href) : ?>
<!--		  <a >-->
<!--              -->
<!--              --><?php //endif?>
<!--	  --><?php
//	     $image = json_decode($result->image)->image_intro;
//	      if (isset($image) && $image != '') {
//		  echo '<div style="float: left; margin: 5px 10px;" ><img src="/'. $image .'" alt="" class="img-fluid news-img"/></div>';
//	      }
//        ?>
<!--		<p class="news-description text-uppercase">-->
<!--		  --><?php //echo $result->title; ?>
<!--		</p>		-->
<!--	    <div class="text-right news-public-date">-->
<!--			<p>-->
<!--				<span>-->
<!--	            --><? //=$result->created?>
<!--			--><?php //// echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
<!--			--><?php ////echo JHtml::_('date',$item->publish_up,'d.m.Y')?>
<!---->
<!--				</span> -->
<!--			</p>-->
<!--		</div>-->
<!--	  </a>-->
<!--			--><?php //// echo $result->title; ?>
<!--			--><? //endif?>
<!--          -->
<!--                    </div>-->
<!--					--><? //endforeach?>
<!--				--><? //endif?>
<!--		--><?php ///// echo $this->pagination->limitstart + $result->count . '. '; ?>
<!---->
<!--	-->
<!---->


<div class="pagination">
    <?php echo $this->pagination->getPagesLinks(); ?>
</div>
