jQuery(function () {

    jQuery('.search-btn').on('click', function () {
        //jQuery('.b-search').slideToggle();
        jQuery('.b-search').fadeIn().css('top', 80);
        jQuery('.b-search__fade').fadeIn();
        jQuery('.b-search__input').focus();
        //$('.main-menu .navbar-form').slideToggle();
    });

    jQuery('.js__search-close').on('click', function () {
        jQuery('.b-search').fadeOut().css('top', 0);
        jQuery('.b-search__fade').fadeOut();
    });

    //main slider function
    (function () {

        var lastIndex = 0;
        var sliderPaused = false;
        var $img_container = jQuery("#slider-image-container img");
        var $slider_links = jQuery(".slider-links");
        var slider_interval = 5000;

        function slider_select($this, e, isClick) {

            jQuery(".site-list-container li.active").removeClass("active");
            jQuery($this).parent().parent().addClass("active");

            e.preventDefault();

            var $target_img = jQuery($this).attr('data-target');
            var view = jQuery($this).data('view');
            var img = jQuery($this).data('img');
            var user = jQuery($this).data('user');
            var seans = jQuery($this).data('seans');
            var god = jQuery($this).data('god');

            if (isClick) {
                var data_index = jQuery($this).data('index');
                lastIndex = data_index - 1;
                sliderPaused = true;

                setTimeout(function () {
                    sliderPaused = false;
                }, 3000);
            }


            if ($target_img) {
                jQuery(".values-info-container").fadeOut(800);
                $img_container.fadeOut(800);

                setTimeout(function () {
                    jQuery('.unikum span').text(user);
                    jQuery('.unikum div span').text(god);
                    jQuery('.logoimg .logoimg2').html(img);

                    jQuery('.prosmotr span').text(view);
                    jQuery('.prosmotr div span').text(god);
                    jQuery('.logoimg .logoimg2').html(img);

                    jQuery('.seans span').text(seans);
                    jQuery('.seans div span').text(god);
                    jQuery('.logoimg .logoimg2').html(img);

                    $img_container.css('visibility', '');
                    jQuery('.values-info-container').css('visibility', '');
                    $img_container.attr("src", '/' + $target_img);
                }, 800);

                jQuery(".values-info-container").fadeIn(800);
                $img_container.fadeIn(800);
            }
        }

        $slider_links.on('click', function (e, isClick) {
            slider_select(this, e, !isClick);
        });


        setTimeout(function () {
            jQuery($slider_links[0]).trigger('click', [true]);
            lastIndex++;
        }, 800);


        setInterval(function () {
            if (!sliderPaused) {
                jQuery($slider_links[lastIndex]).trigger('click', [true]);
                lastIndex++;

                if (lastIndex === $slider_links.length) {
                    lastIndex = 0;
                }
            }
        }, slider_interval);

    })();

    (function () {
        $(".download-document-link").click(function(){
            var $articleId = $(this).data('id');

            $.post('/index.php?option=com_content&task=article.watcher_file', { articleId: $articleId }, function(data) {

            });
        });

    })();

    //accordions reports,jobs
    (function () {

        var toggleMethods = {

            reports: function (e) {
                var $collapsiblePanel = e.parents(".card").find(".collapse");
                var $collapsedPanel = $(".collapse.show");

                if (e.hasClass("active")) {
                    e.removeClass("active");
                    $collapsedPanel.removeClass("show");
                } else {

                    $(".accordion-toggle-btn.active").removeClass("active");
                    $collapsedPanel.removeClass("show");

                    if ($collapsiblePanel) {
                        e.addClass("active");
                        $collapsiblePanel.addClass("show");
                    }
                }
            }

        };

        $(".accordion-toggle-btn").click(function () {
            var $selectItem = $(this);

            if ($selectItem.attr("mode")) {
                toggleMethods[$selectItem.attr("mode")] && toggleMethods[$selectItem.attr("mode")]($selectItem);
                return;
            }


            var $detailContainer = $selectItem.parent().parent();
            var $extraDetailContainer = $(".extra-parameters-block");

            if ($selectItem.hasClass("active")) {
                $selectItem.removeClass("active");
                $extraDetailContainer.not("hide").addClass("hide");
            } else {
                $(".accordion-toggle-btn.active").removeClass("active");
                $selectItem.addClass("active");

                $extraDetailContainer.addClass("hide");
                $detailContainer.next(".extra-parameters-block.hide").removeClass("hide");
            }


        });

    })();


});