<?php defined("_JEXEC") or die(); ?>


<?php

$templateparams = JFactory::getApplication()->getTemplate(true)->params;
$language = JFactory::getLanguage()->get('tag');
$site_logo = $templateparams->get('site_logo');

$doc = JFactory::getDocument();
$menu = JFactory::getApplication()->getMenu();

$activePage = $menu->getActive()->id;
$defaultPage = $menu->getDefault()->id;

$doc->addStyleSheet(JUri::base() . 'templates/' . $doc->template . '/css/bootstrap4.css');
$doc->addStyleSheet(JUri::base() . 'templates/' . $doc->template . '/css/font-awesome.css
');
//$doc->addStyleSheet(JUri::base().'templates/'.$doc->template.'/css/bootstrap3.css');
$doc->addStyleSheet(JUri::base() . 'templates/' . $doc->template . '/css/main.css');
$doc->addStyleSheet(JUri::base() . 'templates/' . $doc->template . '/css/donttouch.css');

$doc->addScript(JUri::base() . 'templates/' . $doc->template . '/js/jquery-2.0.0.min.js');
//$doc->addScript(JUri::base().'templates/'.$doc->template.'/js/bootstrap4.min.js');
//$doc->addScript(JUri::base().'templates/'.$doc->template.'/js/bootstrap3.js');

$doc->addScript(JUri::base() . 'templates/' . $doc->template . '/js/main.js');


if ($doc->countModules("position-0")) {
    $showMainMenu = TRUE;
}

if ($doc->countModules("position-4 or position-5 or position-6")) {
    $showCenterService = TRUE;
}

if (!isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
}

?>

<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=1450">
    <jdoc:include type="head"/>

    <script>
        window.csrf = {csrf_token: '<?php echo $_SESSION['csrf_token']; ?>'};
        $.ajaxSetup({
            data: window.csrf
        });
    </script>
</head>

<body>
<div class="container-fluid no-padding">
    <!-- top container --->

    <div class="top-container">
        <div class="d-flex justify-content-center">
            <a href="/" class="navbar-brand mt-2 ml-5 mr-5">
                <img src="<?php echo JUri::base(); ?>templates/<?php echo $doc->template; ?>/images/logo.png"
                     class="logo" alt="logo"/>
            </a>
            <div class="nav-scroller mt-2 mb-2 w-50 align-self-center" style=''>
                <nav class="nav d-flex justify-content-between navbar-expand-lg">
                    <jdoc:include type="modules" name="position-0" style="simple_mobile_empty"/>
                </nav>
            </div>

            <div class="nav-scroller align-self-center text-center mt-2 mb-2 ml-5 mr-5 mail-link">
                <a target="_blank" href="mailto:info@kazcontent.kz">info@kazcontent.kz</a>
            </div>

            <div class="nav-scroller align-self-center text-center mt-2 mb-2 ml-5 mail-link ">

                <a href="#" class="search-btn ml-2 mr-4">


                    <img src="<?php echo JUri::base(); ?>templates/<?php echo $doc->template; ?>/images/search-btn.png"
                         alt="#"/>


                </a>
                <?php if ($doc->countModules('position-13')) : ?>
                    <jdoc:include type="modules" name="position-13"/>
                <?php endif; ?>


            </div>

        </div>
        <div class='clear'></div>
        <? if ($doc->countModules('position-11')) : ?>
            <jdoc:include type="modules" name="position-11"/>
        <?php endif; ?>

    </div>
</div>

<div class="content">

    <? //print_r($activePage)?>



    <?php if ($activePage == 101 || $activePage == 102 || $activePage == 103 || $activePage == 104 || $activePage == 108 || $activePage == 107 || $activePage == 110 || $activePage == 109 || $activePage == 116 || $activePage == 113 || $activePage == 119 || $activePage == 120 || $activePage == 121 || $activePage == 122 || $activePage == 123 || $activePage == 124): ?>


    <? endif ?>
    <?php if ($activePage == 108) : ?>
        <?php if ($doc->countModules('position-12')) : ?>
            <jdoc:include type="modules" name="position-12"/>
        <?php endif; ?>
    <? endif ?>


    <!---------------------блок 2------------------------>


    <?php if ($activePage == $defaultPage || $activePage == 113 || $activePage == 116) : ?>


        <div class="slider-container">
            <?php if ($doc->countModules('position-10')) : ?>
                <jdoc:include type="modules" name="position-10"/>
            <?php endif; ?>
        </div>


        <?php if ($doc->countModules('position-3')) : ?>
            <jdoc:include type="modules" name="position-3" style="style2"/>
        <?php endif; ?>


        <div class="section-container">
            <div class="section-body company-factor-values-container">
                <div class="container">
                    <div class="row py-5 py-6">
                        <div class="col-md-4">
                            <div class="card bg-transparent text-center border-0">
                                <div class="card-body">
                                    <div class="company-factor-value">
                                        18 млн
                                    </div>
                                    <div class="company-factor-description">
                                        <? if ($language == 'ru-RU'): ?>уникальных<BR> пользователей
                                        <? elseif ($language == 'kk-KZ'): ?>бірегей пайдаланушылар
                                        <? else: ?>уникальных<BR> пользователей
                                        <? endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card bg-transparent text-center border-0">
                                <div class="card-body">
                                    <div class="company-factor-value">
                                        75 млн
                                    </div>
                                    <div class="company-factor-description">
                                        <? if ($language == 'ru-RU'): ?>просмотров<BR> страниц
                                        <? elseif ($language == 'kk-KZ'): ?>парақша қаралымдары
                                        <? else: ?>просмотров<BR> страниц
                                        <? endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card bg-transparent text-center border-0">
                                <div class="card-body">
                                    <div class="company-factor-value">
                                        45 млн
                                    </div>
                                    <div class="company-factor-description">
                                        <? if ($language == 'ru-RU'): ?>сеансов
                                        <? elseif ($language == 'kk-KZ'): ?>сеанстар
                                        <? else: ?>сеансов
                                        <? endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($doc->countModules('position-5')) : ?>

            <jdoc:include type="modules" name="position-5"/>
        <?php endif; ?>
    <? endif ?>


    <jdoc:include type="component"/>

    <!-----------------footer------------------------->
    <div class="footer-container">


        <div class="container">
            <div class="row">
                <div class="col-md-6">

                    <p class="ml-5 mr-5 footer-label">
                        <?php if ($doc->countModules('position-13')) : ?>
                            <jdoc:include type="modules" name="position-13"/>
                        <?php endif; ?>

                    <p><img src="templates/kazkontent/images/footer-logo.png"/></p>
                    <? if ($language == 'ru-RU'): ?>Copyright © 2019, АО «Казконтент». <br>Все права защищены.
                    <? elseif ($language == 'kk-KZ'): ?>Copyright © 2019, АО «Қазконтент».
                        <br>Барлық құқықтары қорғалған.
                    <? else: ?>Copyright © 2019, АО «Казконтент». <br>Все права защищены.
                    <? endif ?>
                    </p>
                </div>

                <div class="col-md-3">
                    <p></p>
                    <span class="footer-label">
	                	<? if ($language == 'ru-RU'): ?><span style="font-weight: bold;">Адрес:</span> <br>Республика Казахстан, 010000,<br> г. Нур-Султан, ул. Д. Кунаева, 10,<br> БЦ «Emerald Tower»</span>
                    <? elseif ($language == 'kk-KZ'): ?><span style="font-weight: bold;">Мекен-жайы:</span>
                        <br>Қазақстан Республикасы, 010000,<br> Нұр-Сұлтан қ., Д. Қонаев к., 10,
                        <br> «Emerald Tower» БО</span>
                    <? else: ?><span style="font-weight: bold;">Адрес:</span> <br>Республика Казахстан, 010000,
                        <br> г. Нур-Султан, ул. Д. Кунаева, 10,<br> БЦ «Emerald Tower»</span>
                    <? endif ?>
                </div>
                <div class="col-md-3">
                    <p></p>
                    <span class="footer-label"><span
                                style="font-weight: bold;">Телефон:</span><br> +7 (7172) 79-82-00 <br>E-mail: <a
                                href="mailto:info@kazcontent.kz" target="_blank">info@kazcontent.kz</a></span>
                </div>
            </div>
        </div>
    </div>

</div>


<script>

</script>
</body>
</html>
