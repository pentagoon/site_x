<?php
abstract class SdcHelper
{
		public function check($name) {
        $db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from($db->quoteName('#__sdc'));
		$query->where($db->quoteName('name')." = ".$db->quote($name));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if (!count($result) > 0)
        {
		$query = $db->getQuery(true);
		$columns = array('name', 'clicks');
		$values = array($db->quote($name), 0);
		$query
		->insert($db->quoteName('#__sdc'))
		->columns($db->quoteName($columns))
		->values(implode(',', $values));
		$db->setQuery($query);
		$db->execute();
		 }
		}
		
		public function fsize($bytes, $decimals = 0) {
		$sz = array('b','kb','mb','gb');
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
		}
		
    	public function click($name) {
        $db =& JFactory::getDBO();
		$query = $db->getQuery(true)
			->update('#__sdc')
			->set('clicks = (clicks + 1)')
			->where("name = '$name'");
		$db->setQuery($query);
		$db->query(); }	
		
		public function showclick($name) {
        $db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('clicks');
		$query->from($db->quoteName('#__sdc'));
		$query->where($db->quoteName('name')." = ".$db->quote($name));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
		}
		
		public function plural($number) {
		$suffix = array("PLG_CONTENT_SDC_SITE_CLICKS_COUNTER","PLG_CONTENT_SDC_SITE_CLICKS_COUNTER2","PLG_CONTENT_SDC_SITE_CLICKS_COUNTER3");
		$keys = array(2, 0, 1, 1, 1, 2);
		$mod = $number % 100;
		$suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
		return $suffix[$suffix_key];	
		}
		
		public function downloadfile($url) {
		header("Location: $url ");
		die; 
		}		
}
?>
