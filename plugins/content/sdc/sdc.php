<?php
defined('_JEXEC') or die;
require_once dirname(__FILE__).'/helper.php'; 
$lang = JFactory::getLanguage();
$lang->load('plg_content_sdc',JPATH_ADMINISTRATOR);
class plgContentSdc extends JPlugin
{ 
	public function onContentPrepare($context, &$article, &$params, $limitstart) 
	{ $matches = array(); // массив для совпадений
	  if (preg_match_all('/\(sdc.+\)/', $article->text, $matches)) {  // если нет ничего с тегом (sdc xxxxxxx) то дальше не паримся
	  $sdc = array();
	  $downfolder = $this->params->get('downfolder').'/';
	  foreach($matches as $brand => $massiv)
		{
			foreach($massiv  as  $inner_key => $value)
		{   
		    $old = $value;
			$value=trim($value, '()'); // удаляем скобки
		    $value = mb_substr($value, mb_strpos($value, ' ')); // удаляем тег sdc
			$value = str_replace(" ","",$value); // удаляем пробел
			$sdc[$old] = $value;
		}
		}
		foreach ($sdc as $tag => $name) {
		$url = JURI::base().$downfolder.$name; // Внешний путь к файлу
		$loc = $downfolder.$name; // Локальный путь к файлу
		If (JFile::exists($loc)) {
		SdcHelper::check($name);
		$downloadtext = $name;
		if ($this->params->def('show_download_text', 1))
		{
		$downloadtext = JText::_('PLG_CONTENT_SDC_SITE_DOWNLOAD_TEXT');
		}
		if ($this->params->def('show_file_name', 1))
		{
		$downloadtext = $downloadtext."".$name;
		}
		if ($this->params->def('show_filesize', 1))
		{
		$loc = filesize($loc);
		$downloadtext = $downloadtext." (".SdcHelper::fsize($loc).")";
		}
		if ($this->params->def('show_clicks', 1))
		{
		$clicks = SdcHelper::showclick($name); // получаем количество кликов
		$show_clicks_text = JText::_('PLG_CONTENT_SDC_SITE_CLICKS_TEXT');
		$show_clicks_text_after = JText::_(SdcHelper::plural($clicks[0]->clicks));
		$clickshtml = "</br>".$show_clicks_text."".$clicks[0]->clicks."".$show_clicks_text_after;
		}
		if($_GET['download'] == $name){
		SdcHelper::click($name); // записываем в базу +1 клик
		SdcHelper::downloadfile($url); // cкачиваем файл
		}
		$linkcss = $this->params->get('link_css');
		if (isset($linkcss)) { 
		$linkcss = "class='".$linkcss."'";
		}
		$htmlcode = "<a ".$linkcss." href='?download=$name'>".$downloadtext."</a>".$clickshtml; // код вывода на вместо (sdc ......)
		$article->text = str_ireplace($tag, $htmlcode, $article->text); // Подмена текста статьи		
		}
		else {
		$htmlcode = "File ".$name." not found on server"; // Сообщение об отсутствии файла
		$article->text = str_ireplace($tag, $htmlcode, $article->text); // Подмена тега на сообщение об отсутствии файла
		}
		}

	  }
}
}
?>